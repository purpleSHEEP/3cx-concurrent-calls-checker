Console application to read through a 3CX call log and calculate its maximum concurrent calls.
The way it works is to add the 'end time' field to a call and add the call to its 'current calls' list.  Each time it adds a call it first clears out any previous calls that ended before the start of the current call.
You can optionally ask it to output the call log with the end times and 'current number of concurrent calls'.
Usage:
ConcurrentCallsChecker.exe --input calllog.csv
or
ConcurrentCallsChecker.exe --input calllog.csv --output outcalllog.csv
Any feedback will be appreciated.  This is my first publicly posted code of any type.

If you don't want to edit the source code for your own project and just want to use the pre-compiled app you can download it from here:
https://gitlab.com/purpleSHEEP/3cx-concurrent-calls-checker/-/archive/v1.0.0/3cx-concurrent-calls-checker-v1.0.0.zip
