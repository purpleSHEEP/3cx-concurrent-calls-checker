﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcurrentCallsChecker
{
  public class Call
  {
    public DateTime CallTime { get; set; }
    public string CallerId { get; set; }
    public string Destination { get; set; }
    public string Talking { get; set; }
    public DateTime? EndTime { get; set; }
    public int CallCount { get; set; }
  }
}
