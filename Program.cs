﻿using CommandLine;
using CsvHelper;
using Microsoft.Extensions.Configuration;
using System.Globalization;

namespace ConcurrentCallsChecker
{
  public class Program
  {
    class Options
    {
      [Option(
      Required = true,
      HelpText = "CDR file to progress")]
      public string input { get; set; }

      [Option(
      Required = false,
      HelpText = "Output file")]
      public string output { get; set; }
    }

    public static async Task Main(string[] args)
    {
      CommandLine.Parser.Default.ParseArguments<Options>(args)
    .WithParsed(RunOptions)
    .WithNotParsed(HandleParseError);
    }
    static void RunOptions(Options opts)
    {

      if (!String.IsNullOrEmpty(opts.input))
      {
        var records = new List<Call>();
        int callCount = 0;
        var currentCalls = new List<Call>();
        int maxCallCount = 0;

        using (var reader = new StreamReader(opts.input))
        using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
        {
          csv.Read();
          csv.ReadHeader();
          while (csv.Read())
          {
            var record = new Call
            {
              CallTime = csv.GetField<DateTime>("Call Time"),
              CallerId = csv.GetField("Caller ID"),
              Destination = csv.GetField("Destination"),
              Talking = csv.GetField("Talking")
            };
            records.Add(record);
          }
        }

        foreach(var record in records)
        {
          TimeOnly timeOnly;
          TimeOnly.TryParse(record.Talking, out timeOnly);
          if(timeOnly != null)
          {
            record.EndTime = record.CallTime.Add(timeOnly.ToTimeSpan());
            currentCalls.Add(record);
            callCount++;
            maxCallCount = callCount>maxCallCount ? callCount : maxCallCount;
          }
          int callsEnded = currentCalls.Count(c => c.EndTime < record.CallTime);
          callCount = callCount - callsEnded;
          currentCalls = currentCalls.Where(c => c.EndTime >= record.CallTime).ToList();
          record.CallCount = callCount;
        }

        if (!String.IsNullOrEmpty(opts.output))
        {
          using (var writer = new StreamWriter(opts.output))
          using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
          {
            csv.WriteRecords(records);
          }
        }

        Console.WriteLine($"Max concurrent calls: {maxCallCount}");
      }
    }
    static void HandleParseError(IEnumerable<Error> errs)
    {
      Console.WriteLine("Could not parse paramters");
    }
  }
}